# network settings
$ResourceGroupName ="powershell-grp"
$Location="North Europe"

$VirtualNetworkName="app-network"
$VirtualNetworkAddressSpace="10.0.0.0/16"
$SubnetName="SubnetA"
$SubnetAddressSpace="10.0.0.0/24"

$NetworkInterfaceName="linux-interface"

# virtual network
$Subnet=New-AzVirtualNetworkSubnetConfig -Name $SubnetName -AddressPrefix $SubnetAddressSpace

$VirtualNetwork = New-AzVirtualNetwork -Name $VirtualNetworkName -ResourceGroupName $ResourceGroupName `
-Location $Location -AddressPrefix $VirtualNetworkAddressSpace -Subnet $Subnet

$Subnet = Get-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $VirtualNetwork

# network interface - needs subnet object
$NetworkInterface = New-AzNetworkInterface -Name $NetworkInterfaceName `
-ResourceGroupName $ResourceGroupName -Location $Location `
-Subnet $Subnet

# public IP
$PublicIPAddressName="linux-ip"

$PublicIPAddress = New-AzPublicIpAddress -Name $PublicIPAddressName -ResourceGroupName $ResourceGroupName `
-Location $Location -Sku "Standard" -AllocationMethod "Static"

$IpConfig=Get-AzNetworkInterfaceIpConfig -NetworkInterface $NetworkInterface

$NetworkInterface | Set-AzNetworkInterfaceIpConfig -PublicIpAddress $PublicIPAddress `
-Name $IpConfig.Name

$NetworkInterface | Set-AzNetworkInterface

# network security - ssh port 22
$SecurityRule1=New-AzNetworkSecurityRuleConfig -Name "SSH Security Rule" -Description "Allow-SSH" `
-Access Allow -Protocol Tcp -Direction Inbound -Priority 100 `
-SourceAddressPrefix * -SourcePortRange * `
-DestinationAddressPrefix * -DestinationPortRange 22

$NetworkSecurityGroupName="linux-nsg"

$NetworkSecurityGroup=New-AzNetworkSecurityGroup -Name $NetworkSecurityGroupName `
-ResourceGroupName $ResourceGroupName -Location $Location `
-SecurityRules $SecurityRule1

$VirtualNetwork=Get-AzVirtualNetwork -Name $VirtualNetworkName -ResourceGroupName $ResourceGroupName

Set-AzVirtualNetworkSubnetConfig -Name $SubnetName -VirtualNetwork $VirtualNetwork `
-NetworkSecurityGroup $NetworkSecurityGroup `
-AddressPrefix $SubnetAddressSpace

$VirtualNetwork | Set-AzVirtualNetwork

# linux vm

## configuation values
$vmName = "linuxVM"
$vmSize = "Standard_DS2_v2" 
$userName = "linuxusr"

## credentials
$PasswordSecure=ConvertTo-SecureString ' ' -AsPlainText -Force
$Credential = New-Object -TypeName System.Management.Automation.PSCredential `
-ArgumentList $UserName,$PasswordSecure

## network interface
$NetworkInterface= Get-AzNetworkInterface -Name $NetworkInterfaceName -ResourceGroupName $ResourceGroupName

## Define configuration to create OS and image object
$vmConfig = New-AzVMConfig -Name $vmName -VMSize $vmSize

### OS
Set-AzVMOperatingSystem -VM $VmConfig -ComputerName $VmName `
-Credential $Credential -Linux -DisablePasswordAuthentication

### Image
# # Get-AzVMImagePublisher -Location "North Europe" | Where-Object {$_.PublisherName -like "can*"} 
# $publisher = "Canonical"
# # Get-AzVMImageOffer -Location "North Europe" -PublisherName "Canonical" | Where-Object {$_.Offer -like "*ubuntu*"}
# $offer = "UbuntuServer"
# # Get-AzVMImageSku -Location "North Europe" -PublisherName "Canonical" -Offer "UbuntuServer"
# $sku = "19.04-LTS"

Set-AzVMSourceImage -VM $VmConfig -PublisherName "Canonical" `
-Offer "UbuntuServer" -Skus "18.04-LTS" -Version "latest"

## Connect VM with network interface
$Vm=Add-AzVMNetworkInterface -VM $VmConfig -Id $NetworkInterface.Id

## Define boot and connect options to vm object
Set-AzVMBootDiagnostic -Disable -VM $Vm

# creat vm and genearte private key locally
New-AzVM -ResourceGroupName $ResourceGroupName -Location $Location `
-VM $Vm -GenerateSshKey -SshKeyName "Linuxkey"

# post steps
# open puttygen to import the private key and normalize it to valid format
# open putty with public ip and private key as auth method