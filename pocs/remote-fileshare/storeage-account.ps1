
$accountName = "appstore300919822"
$accountKind = 'StorageV2'
$accountSKU = 'Standard_LRS'
$location = 'North Europe'

$blobObject=@{
    FileLocation='.\sample.txt'
    ObjectName='sample1'
   }

ConnectWith-SubscriptionVault
$resourceGrp = New-ResourceGroup -resourceGroupname 'powershell-grp' -Location $location
$storageAccount = New-StorageAccount -accountName $accountName -accountKind $accountKind -accountSKU $accountSKU `
                                     -resourceGroupname $resourceGrp.ResourceGroupName -location $location
$dataContainer = New-Container -containerName 'data' -storeageAccount $storageAccount

Add-FileToContainer -storageAccount $storageAccount -dataContainerName $dataContainer.Name -blobObject $blobObject

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
function ConnectWith-SubscriptionVault {

    # overview:
    # Install-Module Microsoft.PowerShell.SecretManagement
    # Get-Command -Module Microsoft.PowerShell.SecretManagement
    # Get-Command -Module Az.Accounts

    # vault store connection
    $vaultName = "filemanagervault"
    $subscriptionId = "4e229e09-944e-491b-8dab-27d4aed9643d"
    $storeName = "AzKeyVaultStore"
    $KvParams = @{ AZKVaultName= $vaultName; SubscriptionId= $subscriptionId }

    $isVaultRegisterd = Get-SecretVault -Name $storeName
    if ($null -eq $isVaultRegisterd) {
        Register-SecretVault -Module Az.KeyVault -Name $storeName -VaultParameters $KVParams
    }
    
    # read vault values
    if ((Get-SecretInfo -Vault AzKeyVaultStore).length -ge 3) {
        $tenantId = Get-Secret -Name TenantId -AsPlainText
        $appId = Get-Secret -Name AppId -AsPlainText
    
        $appSecretValue = Get-Secret -Name AppSecretValue -AsPlainText
        $secureSecret = $appSecretValue | ConvertTo-SecureString -AsPlainText -Force
    
        $credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $appId, $secureSecret
        Connect-AzAccount -ServicePrincipal -Credential $credential -Tenant $tenantId

        Write-Host "Data: " $tenantId $appId $appSecretValue
    } else {
        write-host "tenant has not enough secrets."
    }
}

function New-ResourceGroup { param([Parameter()][string]$resourceGroupname, [Parameter()][string]$location)

    $resourceGrp = Get-AzResourceGroup -Name $resourceGroupName -Location $location

    if ($null -eq $resourceGrp) {
        write-host 'create new resource group...'
        $resourceGrp = New-AzResourceGroup -Name $resourceGroupName -Location $location
    }
    return $resourceGrp
}
function New-StorageAccount { param([Parameter()][string]$accountName,
                                [Parameter()][string]$accountKind,
                                [Parameter()][string]$accountSKU,
                                [Parameter()][string]$resourceGroupname,
                                [Parameter()][string]$location
)

        $storageAccount=Get-AzStorageAccount -ResourceGroupName $resourceGroupname `
        -Name $accountName
        
        if ($null -eq $storageAccount) {
            write-host 'create new storage account...'
            $storageAccount=New-AzStorageAccount -ResourceGroupName $resourceGroupname `
            -Name $accountName -Kind $accountKind -Location $location `
            -SkuName $accountSKU
        }
        
        return $storageAccount
}

function New-Container { param([Parameter()][string]$containerName, [Parameter()][Object]$storeageAccount)

    $dataContainer = Get-AzStorageContainer -Name $containerName -Context $storeageAccount.Context
    if ($null -eq $dataContainer) {
        write-host 'create new container...'
        $dataContainer = New-AzStorageContainer -Name $containerName -Context $storeageAccount.Context -Permission Blob
    }
    return $dataContainer
}


function Add-FileToContainer { param([Parameter()][Object]$storageAccount, [Parameter()][Object]$dataContainerName, [Parameter()][Object]$blobObject)

    $BlobObject=@{
        FileLocation='sample.txt'
        ObjectName='sample.txt'
    }

    Set-AzStorageBlobContent -Context $storageAccount.Context `
    -Container $dataContainerName `
    -File $BlobObject['FileLocation'] `
    -Blob $BlobObject['ObjectName'] -Force
}