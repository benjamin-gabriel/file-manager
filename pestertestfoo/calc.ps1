function Get-Sum { param([Parameter()][Int]$val1, [Parameter()][Int]$val2)  
    return $val1 + $val2
}

function Get-FileObject {

    $file = Get-Item -Path .\Copy-Files.ps1
    return $file
}

function Get-MyChildItems {

    $files = Get-ChildItem -Path .\*
    return $files
}

function Get-Test() {
    return
}

function Get-StringArray() { param([Parameter()][String]$val1, [Parameter()][String]$val2)  
    $stringArray = $val1, $val2

    if ($stringArray.Count -eq 2) { 
        Write-Host "Array has size 2.."
    }

    return $stringArray

}

$files = (Get-MyChildItems)
# Write-Host $files.GetType() $files.Count # System.Object[] 5

$file = (Get-FileObject)
# Write-Host $file.GetType() # System.IO.FileInfo
# Write-Host "Check: "  $file.FullName  $file.IsReadOnly # Check: C:\projects\powershell-file-manager\calc.ps1False

Describe "Test-ScriptExists Component Tests" {
    It "PS1 exists" {
    ".\Copy-Files.ps1" | Should -Exist
    }

    Context "Calc" {

        It "Add Test" {

            $sum = Get-Sum -val1 12 -val2 13
            $sum | Should -Be 25
        }

        
        It "Add Test Mock" {

            Mock Get-Sum -MockWith {7}
            $sum = Get-Sum -val1 3 -val2 5
            #Write-Host "Sum: " + $sum
            $sum | Should -Be 7
            $sum | Should -Not -Be 8
        }

        It "Mock file" {
            Mock Get-FileObject -MockWith {
                [pscustomobject]@{
                    "FullName"         = "MOCK PATH TEST"
                    "Length"            = 8655
                }
            }

            $file = Get-FileObject
            #Write-Host ($file).FullName
        }

        It "Mock file specific" {
            Mock Get-FileObject { New-MockObject -Type "System.IO.FileInfo" -Properties @{ 
                FullName = "MOCKPATHFILE" 
                IsReadOnly = $True } }

            $file = Get-FileObject
            #Write-Host ($file).FullName
            #Write-Host ($file).IsReadOnly
        }


        It "Array Test" {

            Mock Get-StringArray { $val1, "Mock"}

            $myArray = Get-StringArray -val1 "Ben" -val2 "Gab"
            Write-Host "Array: " $myArray[1] #System.Object[]

        }


    }

}