# Copy-Files PowerShell script

## Description 

Powershell file manager to backup files from one specific source to a destination

## Features
* easy copy process recursivly 
* archiv flag preperation to skip non touched files
* logging backup process

## Getting started

* start the script Copy-Files/Copy-Files.ps1
** type in the source folder
** type in the root destination folder
*** attention: destination root folder has to be exist

## Updates

### 0.1

* local copy process implementation
* component, integration, systemtest added
* track non-copied items in log file

## Documentation
* Microsoft Powershell Core: https://docs.microsoft.com/en-us/powershell/scripting/whats-new/what-s-new-in-powershell-72?view=powershell-7.2
* Pester Quickstart: https://pester.dev/docs/quick-start

## Contact

benjamin.gabriel@outlook.de
