#Import-Module Copy-Files
Import-Module $PSScriptRoot"\Copy-Files.ps1"

$sourcePathTest = "C:\temp\folderATest\"
$destPathTest = "C:\temp\folderBTest\"

Describe "File Management - Component Tests" {

    Context "When script files are available" {
        It "Should ps1 files exist" {
        $PSScriptRoot + "\Copy-Files.ps1" | Should -Exist
        }
    }

    Context "When source files are available" {
        It "Should check 'Test-CopyFile' with archive flag (un)set" {
            $attributes = "Archive"
            (Test-CopyFile -Attributes $attributes) | Should -Be $true
            $attributes = "Normal"
            (Test-CopyFile -Attributes $attributes) | Should -Be $false
        }

        It "Should no item be found in source folder" {
            Mock Get-ChildItem  {}
           # { Get-FilesToCopy -sourcePath "Path to source file" } | Should -Throw "No file items selected to copy." # todo inconsistent behaviour
            
        }
    }

    Context "When source file won't be available" {
        
        It "Should items be found" {

            Mock Get-ChildItem -MockWith {
                [pscustomobject]@{
                    FullName = "a1"
                },
                [pscustomobject]@{
                    FullName = "a2"
                }
            }

            $items = Get-FilesToCopy -sourcePath "Path to source file"
            $items.Count | Should -EQ 2
            $items[1].FullName | Should -Be "a2" 
        }
    }
}

Describe "File Managment - Integration Tests" {

    BeforeAll {
        Write-Host "BeforeAll - Create files and dictionaries."
        New-Item -Path $sourcePathTest -ItemType "directory"
        New-Item -Path $destPathTest -ItemType "directory"
        New-Item -Path ($sourcePathTest + "a.txt") -ItemType "file"
        New-Item -Path ($sourcePathTest + "b.txt") -ItemType "file"
        New-Item -Path ($sourcePathTest + "folderCTest\") -ItemType "directory"
        New-Item -Path ($destPathTest + "folderCTest\") -ItemType "directory"
        New-Item -Path ($sourcePathTest + "folderCTest\c.txt") -ItemType "file"
    }

    It "Should file in source subfolder exist" {
        $testfile = get-item -path "C:\temp\folderATest\folderCTest\c.txt"
        $testFile | Should -Exist
    }   

    It "Should 1 source file be copied to destination sufolder" {
        $testfile = get-item -path "C:\temp\folderATest\folderCTest\c.txt"
        
        $attributes = (Get-ItemProperty -Path $testfile.FullName).Attributes
        
        (Test-CopyFile -Attributes $attributes) | Should -Be $true
        
        Remove-ArchivAttribute -File $testfile
        (Test-CopyFile -Attributes $attributes) | Should -Be $true
        
        Copy-Item -Path $testfile.FullName -Destination $testfile.FullName.Replace($sourcePathTest, $destPathTest) -Recurse -Force
        write-host $testFile.FullName.Replace($sourcePathTest, $destPathTest)
        $testFile.FullName.Replace($sourcePathTest, $destPathTest) | Should -Exist

    }   

    AfterAll {
        Write-Host "AfterAll - Delete files and dictionaries."
        Remove-Item -Path "C:\temp\folderATest" -Recurse
        Remove-Item -Path "C:\temp\folderBTest" -Recurse
    }

}

Describe "File Managment - Systemtest" {
    BeforeAll {
        Write-Host "Before - Create files and dictionaries."
        New-Item -Path $sourcePathTest -ItemType "directory"
        New-Item -Path $destPathTest -ItemType "directory"
        New-Item -Path ($sourcePathTest + "a.txt") -ItemType "file"
        New-Item -Path ($sourcePathTest + "b.txt") -ItemType "file"
        New-Item -Path ($sourcePathTest + "folderCTest\") -ItemType "directory"
        New-Item -Path ($sourcePathTest + "folderCTest\c.txt") -ItemType "file"
    }

    Context "When script to copy files is invoked" {

        It "Should items be copied to destination location" {
        
            Mock Read-Host { 
            
                if ($args[1] -like "*source path*") {
                    return $sourcePathTest
                } else {
                    return $destPathTest
                }
            }

            Start-Sleep -Milliseconds 1000        
            Write-Host "Start the script:"
            & ($PSScriptRoot + "\Copy-Files.ps1")


            $sourceFile = $sourcePathTest + "folderCTest\c.txt"
            $destFile = $destPathTest + "folderCTest\c.txt"

            $sourceFile | Should -Exist
            $destFile | Should -Exist

            (Get-ItemProperty -Path $sourceFile).Attributes | Should -Be "Normal"
            (Get-ItemProperty -Path $destFile).Attributes | Should -Be "Archive"
        }
    }   

    AfterAll {
        Write-Host "After - Delete files and dictionaries."
        Remove-Item -Path "C:\temp\folderATest" -Recurse
        Remove-Item -Path "C:\temp\folderBTest" -Recurse
    }
}