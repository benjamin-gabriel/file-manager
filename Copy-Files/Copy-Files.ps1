function Get-FilesToCopy { param([Parameter()][Object]$sourcePath)  
  try {
    $items = Get-ChildItem $sourcePath'*' -Force -Recurse
    Write-Host ($items.Count.toString() + ' items to prepare...')
  
    if ($items.Count -le 0) {
      throw "No file items selected to copy."
    }

    return $items
  
  } catch {
    Write-Host("An error occurred that could not be resolved: " + $_)
  }
}

function Remove-ArchivAttribute { param([Parameter()][Object]$File)
  $attributeArchiv = [io.fileattributes]::archive

  if ($File -isnot [System.IO.DirectoryInfo]) {
    Get-ItemProperty -Path $File | Set-ItemProperty -Name Attributes -Value ($File.Attributes -bxor $attributeArchiv)
  }
}

function Test-CopyFile { param([Parameter()][Object]$Attributes)
   
  $hasArchivFlag = $false
  if ($attributes -band $attributeArchiv) {
    $hasArchivFlag = $true 
  }
  return $hasArchivFlag
}


#################################### Start Script ####################################
Clear-History

$invocationPath = $MyInvocation.MyCommand.Source | ForEach-Object {$_.replace($MyInvocation.MyCommand.Name ,"")}


$DebugPreference = "Continue"
Write-Host ('##### Backup Script is starting #####')

#$excludedFileList = @("file1","file2")

$sourcePathDefault = "C:\temp\folderA\"
if (!($sourcePath = Read-Host "Source Path [$sourcePathDefault]")) { $sourcePath = $sourcePathDefault }

$destPathDefault = "C:\temp\folderB\"
if (!($destPath = Read-Host "Dest Path [$destPathDefault]")) { $destPath = $destPathDefault }


$position=0
$logfile = $invocationPath + "\logs\" + (Get-Date -UFormat "%Y%m%d%H%M%S").toString() + "_transfer.log"
$attributeArchiv = [io.fileattributes]::archive


$items = Get-FilesToCopy -sourcePath $sourcePath
Add-Content $logfile ("Log session is started")
ForEach($item in $items)
{
    Write-Debug("Traverse: " + $item.FullName)
    $attributes = (Get-ItemProperty -Path $item.FullName).Attributes
    Write-Host $item.GetType()
    if ( (Test-CopyFile -Attributes $attributes) -Or ($item -is [System.IO.DirectoryInfo])) {
      Write-Debug ('File ' + $item.Name + ' will be copied.')
      Copy-Item -Path $item.FullName -Destination $item.FullName.Replace($sourcePath, $destPath) -Recurse -Force
      Add-Content $logfile ("copied file " + $item.FullName)  
      
      Write-Debug ("Remove Archive Attribute for " + $item.Name)
      Remove-ArchivAttribute -File $item
    } 

    # progress
    $position++
    Write-Progress -Activity "FROM '$sourcePath' TO '$destPath'" -Status "Copying File $item" -PercentComplete (($Position/$items.Count)*100)
}

#################################### End Script ####################################